Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Debian Edu (Pure Blend Metapackages and others)
Upstream-Contact: Debian Edu Team <debian-edu@lists.debian.org>
Source: https://salsa.debian.org/debian-edu/debian-edu

Files: debian/*
 debian-edu-tasks.desc
 desktop-directories/DebianEdu.directory
 desktop-directories/Edu-ArcadeGames.directory
 desktop-directories/Edu-Astronomy.directory
 desktop-directories/Edu-BoardGames.directory
 desktop-directories/Edu-CardGames.directory
 desktop-directories/Edu-Chemistry.directory
 desktop-directories/Edu-ComputerScience.directory
 desktop-directories/Edu-Economy.directory
 desktop-directories/Edu-Electricity.directory
 desktop-directories/Edu-Game.directory
 desktop-directories/Edu-Geography.directory
 desktop-directories/Edu-Graphics.directory
 desktop-directories/Edu-Kids.directory
 desktop-directories/Edu-KidsGames.directory
 desktop-directories/Edu-Languages.directory
 desktop-directories/Edu-Literature.directory
 desktop-directories/Edu-LogicGames.directory
 desktop-directories/Edu-Mathematics.directory
 desktop-directories/Edu-Miscellaneous.directory
 desktop-directories/Edu-Music.directory
 desktop-directories/Edu-Physics.directory
 desktop-directories/Edu-Science.directory
 desktop-directories/Edu-StrategyGames.directory
 desktop-directories/Edu-Video.directory
 interdepend.dot
 interdepend-proposal.dot
 Makefile
 menus/gnome-applications.menu
 menus/kf5-applications.menu
 menus/lxde-applications.menu
 menus/lxqt-applications.menu
 menus/mate-applications.menu
 menus/xfce-applications.menu
 menus/applications-merged/astronomy.menu
 menus/applications-merged/chemistry.menu
 menus/applications-merged/computerscience.menu
 menus/applications-merged/economy.menu
 menus/applications-merged/electricity.menu
 menus/applications-merged/game.menu
 menus/applications-merged/geography.menu
 menus/applications-merged/graphics.menu
 menus/applications-merged/kids.menu
 menus/applications-merged/languages.menu
 menus/applications-merged/literature.menu
 menus/applications-merged/math.menu
 menus/applications-merged/miscellaneous.menu
 menus/applications-merged/music.menu
 menus/applications-merged/physics.menu
 menus/applications-merged/science.menu
 menus/applications-merged/video.menu
 sources.list.bookworm
 sources.list.bullseye
 sources.list.README
 sources.list.stable
 sources.list.unstable
 tasks/astronomy
 tasks/chemistry
 tasks/common
 tasks/desktop-cinnamon
 tasks/desktop-gnome
 tasks/desktop-kde
 tasks/desktop-lxde
 tasks/desktop-lxqt
 tasks/desktop-mate
 tasks/desktop-other
 tasks/desktop-xfce
 tasks/development
 tasks/electronics
 tasks/geography
 tasks/graphics
 tasks/highschool
 tasks/language
 tasks/laptop
 tasks/logic-games
 tasks/ltsp-server
 tasks/main-server
 tasks/mathematics
 tasks/misc
 tasks/music
 tasks/networked
 tasks/networked-common
 tasks/physics
 tasks/preschool
 tasks/primaryschool
 tasks/roaming-workstation
 tasks/secondaryschool
 tasks/standalone
 tasks/thin-client
 tasks/video
 tasks/workstation
 tasks.ctl
Copyright: 2001, Raphael Hertzog <hertzog@debian.org>
  2001-2018, Petter Reinholdtsen <pere@hungry.com>
  2006-2021, Holger Levsen <holger@layer-acht.org>
  2011-2018, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
  2013-2021, Wolfgang Schweer <w.schweer@gmx.de>
License: GPL-2+

Files: pixmaps/edu-geography.png
 pixmaps/edu-economy.png
 pixmaps/edu-astronomy.png
 pixmaps/linexedu.png
 pixmaps/edu-literature.png
 pixmaps/edu-design.png
 pixmaps/edu-kids.png
 pixmaps/edu-chemistry.png
 pixmaps/edu-sports.png
 pixmaps/edu-languages.png
 pixmaps/edu-music.png
 pixmaps/edu-miscellaneous.png
 pixmaps/edu-graphics.png
 pixmaps/edu-mathematics.png
 pixmaps/edu-physics.png
 pixmaps/edu-electricity.png
Copyright: 2007, Junta de Extremadura
  2002 (and following years), KDE Artists
License: LGPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Genaral Public License for more details.
 .
 You should have received a copy og the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public
 License along with this library. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU Library General
 Public License version 2 can be found in "/usr/share/common-licenses/LGPL-2".
