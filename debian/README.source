About DebianEdu metapackages
----------------------------

The metapackages should always be installable on a Debian system.
Thus no dependency on a non-available package (i.e. a package available
outside of Debian) have been used. However such dependencies have
been downgraded to Suggests.

Sometimes some non-free packages are suggested when no good free
alternatives are known. You're encouraged to find good free
replacements.

If you want to help, please check http://wiki.debian.org/DebianEdu


Building a new version of this package
--------------------------------------

Note: When releasing a new version of this package, be sure to cause
debian/control to be updated to reflect any changes (added or removed
packages) in the Debian archive. Start by doing:

   make dist

This creates a source package in the parent directory which is tagged
by the same version number as in the changelog.  Unpack this source
package at any other place because the auto generated debian/control
file should not be checked into Git.  Then use for instance
debuild / pdebuild (but not git-buildpackage) to build the package.

The distribution field in debian/changelog has to have a valid value
(UNRELEASED is not allowed, it should be bookworm atm), since it is used
(directly from Git) for the CD and DVD builds.

This is documented in more detail in the documentation from blends-dev
which is available online at

    https://blends.debian.org/blends/apa.html
and
    https://blends.debian.org/blends/apb.html


How to add packages to the tasks
--------------------------------

See below for a longer explanation.

There are several options when adding packages to a task:

Depends:

  These packages are to be installed when the task is installed.  Without these
  the task is not fulfilling its purpose

Recommends:

  These packages are nice to have packages, which will be installed when they
  are available, and ignored if not.

Suggests:

  These packages will not be installed if available.

Ignore:

  The packages listed here will not be installed by default, but the CD build
  system will try to make sure the packages are included on the DVD or CD.

Avoid:

  The packages listed here should be kept out of the CDs/DVDs.


Dependency graph for the tasks
------------------------------

The task inter-dependencies are documented in the interdepend.dot dot file.
To view the dependency graph, run 'dotty interdepend.dot'.


How to add packages to the Debian Edu DVD
-----------------------------------------

Here is a bit longer intro on how to add packages to the Debian Edu DVD.
This assumes the package which is to be added already exists in the
Debian APT repository, and that you have write access to the debian-edu
source repository on salsa.debian.org.

 1) Fetch the debian-edu source package from GIT

     git clone git@salsa.debian.org:debian-edu/debian-edu

 2) Enter your just cloned debian-edu repository

     cd debian-edu

 3) Edit the task you want to modify, adding packages (more on this
    later), I use the education-common task as an example.

     emacs tasks/common

 4) Update the package changelog, documenting the change.

     emacs debian/changelog

 5) Check the change, make sure nothing unwanted is modified.

     git diff

 6) Test if the package still builds, as described above in the "Building a new
    version of this package" paragraph.

 7) When the change is complete, commit it to Git, with an explanation
    on the change, for example the same description stored in the
    changelog:

     git commit -a -m \
       "Depend on openssh-server, to make sure ssh is enabled on all machines"\
       debian/changelog tasks/common

 8) Finally, push the changes to salsa:

     git push


The task files include blocks like this:

  Depends:     procinfo, sysutils
  Why:         Needed to access procinfo for debugging
  Responsible: Petter Reinholdtsen
  NeedConfig:  no

Instead of 'depends', one can use 'recommends', 'suggests', 'ignore'
and 'avoid'.  They affect how the package is included on the DVD and
CDs, and if they are installed by default or not.  The depends,
recommends and suggests are listed in the metapackages.  The packages
listed as depends are included first on the DVD and CDs, and are
installed if the installation system find the packages.  The
recommended packages are included next and installed if found by the
installation system, but are at the moment unlikely to make it onto
the CDs.  Recommends make it into to DVD.  The suggested packages are
unlikely to make it into the CDs, but make it into the DVD.  They are
not installed by default.  The ignored packages are included on the
DVD, but are not installed.  The avoided packages are excluded from
the DVD and CDs, and thus not installed nor available.

New packages you want to have included should start as ignored
packages, to get them on the DVD but not installed by default.  When
they are found to be useful, we can update them to suggests (to make
them more visible, but not installed by default).  If they are really
useful, possible to preseed and work out of the box, we can move them
to recommend or depend, but before this is done, we need to verify
that the disk space required is available in the default installation.

We are currently using 1.6 GiB of the 4.4 GiB DVD capacity.  The rest
of the DVD content is popular packages according to popcon.debian.org
(so the most commonly used packages are included first).  This mean we
have heap of space available for packages.

Do not be afraid of listing packages as ignore, to get them on our
radar.

