Format: https://blends.debian.org/blends/1.1
Task: Main-server
Architecture: any
Test-edu-profile: Main-Server
Description: Debian Edu main server packages
 A metapackage containing dependencies for packages required on all
 main server installations in the Debian Edu Blend.

Recommends:
 education-networked,

Recommends:
 iptables,

Recommends:
 cups,
 cups-bsd,
 cups-pdf,
 foomatic-db-compressed-ppds,
 hp-ppd,
 hpijs-ppds,

Recommends:
 bind9

Suggests:
 nslint,
 dnswalk,
 dlint,
NeedConfig: yes - set a few well known DNS names, and more.

Recommends:
 ldap2zone
NeedConfig: yes - touch the corresponding bind zone file

Recommends:
 isc-dhcp-server-ldap,
 debian-installer-11-netboot-i386,
 debian-installer-11-netboot-amd64,
 tftpd-hpa,
 ipxe,
NeedConfig: yes - set local IP range, DNS name and default routing

Recommends:
 memtest86+,

Recommends:
 slapd,
 libnss3-tools,
NeedConfig: yes - generate certificates for ssl/tls support

Recommends:
 dovecot-imapd,

Suggests:
 dovecot-pop3d,
NeedConfig: yes - configure POP3/IMAP server

Recommends:
 dovecot-gssapi,
NeedConfig: yes - hook into Kerberos

Recommends:
 exim4-daemon-heavy,
NeedConfig: yes - set up SMTP service

Recommends:
 mutt,

Recommends:
 links,

Recommends:
 nfs-kernel-server,
NeedConfig: yes - export directories via /etc/exports

Recommends:
 samba,
 samba-common-bin,
 makepasswd,
 tdb-tools,
 smbclient,
 avahi-utils,
NeedConfig: yes - configure standalone Samba server w/ usershares enabled

Recommends:
 apache2,

Suggests:
 apache2-doc,
 webalizer,

Recommends:
 squid,

Suggests:
 calamaris,
 squidguard,
 sarg,
NeedConfig: yes

Suggests:
 dsh,
NeedConfig: yes - configure host groups

Recommends:
 ntp,
NeedConfig: yes - set NTP server (client: ntp.intern, server: ntp.somewhere)

Recommends:
 slbackup,
 slbackup-php,
NeedConfig: yes - debconf (only the slbackup package)

Recommends:
 munin,
NeedConfig: yes - done automatically by preseeding sitesummary

Recommends:
 icinga2,
 icingaweb2,
 mariadb-server,
 monitoring-plugins-standard,
 nagios-nrpe-plugin,
NeedConfig: yes - done via preseeding: enable icinga-ido-mysql connection.

Recommends:
 sitesummary,

Suggests:
 ocsinventory-server,
 ocsinventory-reports,
 default-mysql-client,

Recommends:
 gosa,
 gosa-schema,
 gosa-plugin-samba,
 gosa-plugin-ldapmanager,
 gosa-plugin-sudo,
 gosa-plugin-sudo-schema,
 gosa-plugin-dhcp,
 gosa-plugin-dhcp-schema,
 gosa-plugin-dns,
 gosa-plugin-dns-schema,
 gosa-help-en,
 gosa-help-de,
 gosa-help-fr,
 gosa-help-nl,
 gosa-plugin-netgroups,
 gosa-plugin-goto,
 gosa-plugin-pwreset,
NeedConfig: yes

Recommends:
 krb5-kdc,
 krb5-admin-server,
 krb5-kdc-ldap,
 libsasl2-modules-gssapi-mit,
NeedConfig: yes - configure realm and the LDAP server as backend

Recommends:
 libapache2-mod-auth-gssapi,
NeedConfig: yes - individual services like Nagios need to use the apache module

Suggests:
 krb5-doc,
 libsasl2-modules-ldap,

